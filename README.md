Laurent Fainsin \
1A, SN, 2020-2021 \
note: 18/20

# Building

```
$ colormake
```

# Cleaning

```
$ make clean
```

# Testing

```
$ make test
```

# Tree

. \
├── build \
├── doc \
│ ├── inp_n7.png \
│ ├── PageRanks-Example.jpg \
│ ├── raffinages.txt \
│ ├── rapport&period;md \
│ ├── rapport.pdf \
│ ├── rapport.tex \
├── Makefile \
├── README&period;md \
├── src \
│ ├── google.adb \
│ ├── google.ads \
│ ├── pagerank.adb \
│ ├── quicksort.adb \
│ ├── quicksort.ads \
│ ├── vector.adb \
│ └── vector.ads \
└── test_pagerank.bash
