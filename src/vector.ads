with Ada.Text_IO; use Ada.Text_IO;

package Vector is

    -- on crée 2 sous modules pour chaque type de base
    -- créer un seul et même module pour les deux types à la fois est compliqué à réaliser.

    generic

        type T_Entier is range <>;
        Capacite: Positive;
    
    package Entier is

        -- on permet l'affichage direct des T_Entier
        package Text_T_Entier is
            new Ada.Text_IO.Integer_IO(Num => T_Entier);
        use Text_T_Entier; 

        type T_Vecteur is array (0..Capacite-1) of T_Entier;

        -- permet d'initialiser le vecteur avec pour tout i, vec[i] = value
        procedure initialize(vec: in out T_Vecteur; value: in T_Entier);

        -- permet d'initialiser le vecteur avec pour tout i, vec[i] = i
        procedure identity(vec: in out T_Vecteur);

        -- comme en proba, on calcule "la fonction de répartition" du vecteur
        procedure repartition(vec: in out T_Vecteur);

        -- permet de renverser le vecteur, équivalent vec[::-1] en python
        procedure flip(vec: in out T_Vecteur);

        -- print to console
        procedure put(vec: in T_Vecteur);
        -- print to file
        procedure put(file: in Ada.Text_IO.File_Type; vec: in T_Vecteur);

    end Entier;



    generic

        type T_Reel is digits <>;
        Capacite: Positive;

    package Reel is

        -- on permet l'affichage direct des T_Reel
        package Text_T_Reel is
            new Ada.Text_IO.Float_IO(Num => T_Reel);
        use Text_T_Reel;       

        type T_Vecteur is array (0..Capacite-1) of T_Reel;

        -- permet d'initialiser le vecteur avec pour tout i, vec[i] = value
        procedure initialize(vec: in out T_Vecteur; value: in T_Reel);

        -- somme toutes les valeurs du vecteur
        function sum(vec: in T_Vecteur) return T_Reel;

        -- permet de renverser le vecteur, équivalent vec[::-1] en python
        procedure flip(vec: in out T_Vecteur);

        -- print to console
        procedure put(vec: in T_Vecteur);
        -- print to file
        procedure put(file: in Ada.Text_IO.File_Type; vec: in T_Vecteur);

    end Reel;

end Vector;
