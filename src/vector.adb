package body Vector is

    package body Entier is

        procedure initialize(vec: in out T_Vecteur; value: in T_Entier) is
        begin
            for i in 0..Capacite-1 loop
                vec(i) := value;
            end loop;
        end initialize;

        procedure identity(vec: in out T_Vecteur) is
        begin
            for i in 0..Capacite-1 loop
                vec(i) := T_Entier(i);
            end loop;
        end identity;

        procedure repartition(vec: in out T_Vecteur) is
        begin
            for i in 1..Capacite-1 loop
                vec(i) := vec(i-1) + vec(i);
            end loop;
        end repartition;

        procedure flip(vec: in out T_Vecteur) is
            tmp: T_Entier;
        begin
            for i in 0..Capacite/2-1 loop
                tmp := vec(i);
                vec(i) := vec(Capacite-i-1);
                vec(Capacite-i-1) := tmp;
            end loop;
        end flip;

        procedure put(vec: in T_Vecteur) is
        begin
            for i in 0..Capacite-1 loop
                put(vec(i), 1); new_line;
            end loop;
        end put;

        procedure put(file: in Ada.Text_IO.File_Type; vec: in T_Vecteur) is
        begin
            for i in 0..Capacite-1 loop
                put(file, vec(i), 1);
                new_line(file);
            end loop;
        end put;

    end Entier;

    package body Reel is

        procedure initialize(vec: in out T_Vecteur; value: in T_Reel) is
        begin
            for i in 0..Capacite-1 loop
                vec(i) := value;
            end loop;
        end initialize;

        function sum(vec: in T_Vecteur) return T_Reel is
            s: T_Reel := 0.0;
        begin
            for i in 0..Capacite-1 loop
                s := s + vec(i);
            end loop;
            return s;
        end sum;

        procedure flip(vec: in out T_Vecteur) is
            tmp: T_Reel;
        begin
            for i in 0..Capacite/2-1 loop
                tmp := vec(i);
                vec(i) := vec(Capacite-i-1);
                vec(Capacite-i-1) := tmp;
            end loop;
        end flip;

        procedure put(vec: in T_Vecteur) is
        begin
            for i in 0..Capacite-1 loop
                put(vec(i), Fore=>1, Aft=>10); new_line;
            end loop;
        end put;

        procedure put(file: in Ada.Text_IO.File_Type; vec: in T_Vecteur) is
        begin
            for i in 0..Capacite-1 loop
                put(file, vec(i), Fore=>1, Aft=>10);
                new_line(file);
            end loop;
        end put;

    end Reel;

end Vector;
