with Vector;

generic

    -- Vecteur contenant les élements du même type que la matrice
    with package Vector_Element is new Vector.Reel(<>);

    -- Vecteur contenant des élements de type entier
    with package Vector_Index is new Vector.Entier(<>);    

package Quicksort is

    use Vector_Element;
    use Vector_Index;

    procedure sort(vec: in out Vector_Element.T_Vecteur;
                    low: Natural := 0; high: Natural := Vector_Element.Capacite-1);

    procedure sort(vec: in out Vector_Element.T_Vecteur; vec_index: in out Vector_Index.T_Vecteur;
                    low: Natural := 0; high: Natural := Vector_Element.Capacite-1);

end Quicksort;