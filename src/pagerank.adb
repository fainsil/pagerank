with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO.Text_Streams;

with Vector;
with Google;
with Quicksort;

procedure pageRank is
    
    -- défition d'exception pour gérer le parsing des arguments.
    ERROR_args: Exception;
    ERROR_alpha: Exception;
    ERROR_iteration: Exception;
    ERROR_filename: Exception;
    INFO_tips: Exception;
    INFO_help: Exception;

    -- définition du type T_Reel
    Type T_Reel is digits 18;
    -- on utilise le module générique Float_IO pour pouvoir afficher T_Reel directement
    package Text_T_Reel is
        new Ada.Text_IO.Float_IO(Num => T_Reel);
    use Text_T_Reel;

    -- permet de faire un retour chariot pour écrire plusieurs fois sur la même ligne, pas possible avec un put
    stdout: constant Ada.Text_IO.File_Type := Ada.Text_IO.Standard_Output;





    -- procédure pour récupérer les arguments de la ligne de commande
    procedure get_args(filename: in out Unbounded_String;
                       ite_max: in out Natural;
                       alpha: in out T_Reel;
                       naif: in out Boolean) is
        i: Natural := 1;
    begin
    
        -- on affiche tous les arguments
        put("Argument_Count = "); put(Argument_Count, 1); new_line;
        for i in 1..Argument_Count loop
            Put("Argument(");
            Put(i, 0);
            Put(") = ");
            Put_line(Argument(i));
        end loop;
        
        new_line;

        -- on vérifie d'abord que le nombre d'arguments est cohérent
        if not(0 < Argument_Count and Argument_Count <= 7) then
            raise ERROR_args;
        else -- sinon on parse les arguments
            loop
                if Argument(i) = "-h" or Argument(i) = "--help" then
                    put_line("parsed help");
                    raise INFO_help;

                elsif Argument(i) = "-n" or Argument(i) = "--naif" then
                    naif := True;
                    i := i + 1;
                    put_line("parsed naif");

                elsif Argument(i) = "-a" or Argument(i) = "--alpha" then
                    alpha := T_Reel'Value(Argument(i+1));
                    if alpha < 0.0 or alpha > 1.0 then
                        raise ERROR_alpha;
                    end if;
                    i := i + 2;
                    put_line("parsed alpha");

                elsif Argument(i) = "-i" or Argument(i) = "--ite-max" then
                    ite_max := Natural'Value(Argument(i+1));
                    if ite_max > 150 then
                        raise ERROR_iteration;
                    end if;
                    i := i + 2;
                    put_line("parsed ite_max");

                elsif Argument(i)'Length > 4 and then Argument(i)(Argument(i)'Last-3 .. Argument(i)'Last) = ".net" then
                    filename := To_Unbounded_String(Argument(i)(Argument(i)'First .. Argument(i)'Last-4));
                    i := i + 1;
                    put_line("parsed filename");

                else
                    new_line;
                    put("Argument: '");
                    put(Argument(i));
                    put("' non reconnu");
                    new_line;
                    raise INFO_tips;

                end if;
                exit when i > Argument_Count;

            end loop;

            if Length(filename) = 0 then
                raise ERROR_filename;
            end if;

        end if;

        new_line;
        put("alpha = "); put(alpha, 1); new_line;
        put("naif = "); put(Boolean'Pos(naif), 1); new_line;
        put("ite_max = "); put(ite_max, 1); new_line;
        put("filename = "); put_line(To_String(filename));
        
    exception

        when CONSTRAINT_ERROR =>
            if Argument(i) = "-a" or Argument(i) = "--alpha" then
                raise ERROR_alpha;
            elsif Argument(i) = "-i" or Argument(i) = "--ite-max" then
                raise ERROR_iteration;
            else
                put_line("Unexpected constraint_error");
                raise ERROR_args;
            end if;

    end get_args;





    -- définition des arguments
    filename: Unbounded_String;
    ite_max: Natural := 150;
    naif: Boolean := False;
    alpha: T_Reel := 0.85;

    -- définition des variables pour créer les matices/vecteurs
    N: Positive;
    N_links: Integer := -1;
    file: Ada.Text_IO.File_Type;

begin

    -- on récupère les arguments de la ligne de commande
    get_args(filename, ite_max, alpha, naif);
    put_line("parsed successfully arguments"); new_line;

    -- on ouvre le fichier .net
    open(file, In_File, To_String(filename & ".net"));
    put("opened "); put(To_String(filename & ".net")); new_line;

    -- on récupère le nombre de pages (N)
    get(file, N);
    put("N = "); put(N, 1); new_line;

    -- on récupère le nombre de liens (N_Links)
    while not End_Of_File(file) loop
        skip_line(file);
        N_links := N_links + 1;
    end loop;
    put("N_links = "); put(N_links, 1); new_line;
    
    -- on calcule la densité
    put("densité = "); put(T_Reel(N_Links)/T_Reel(N)**2, 1); new_line;

    new_line;

    -- on renvient au début du fichier
    reset(file, In_File);
    -- au saute la première ligne pour s'apprêter à lire le réseau
    skip_line(file);

    declare

        -- on peut maintenant créer nos vecteurs
        package Vector_Entier is
            new Vector.Entier(T_Entier => Natural, Capacite => N);
        package Vector_Reel is
            new Vector.Reel(T_Reel => T_Reel, Capacite => N);

        use Vector_Entier;
        use Vector_Reel;

        pi: Vector_Reel.T_Vecteur;
        pi_index: Vector_Entier.T_Vecteur;

        -- on instancie aussi l'algorithme QuickSort
        package Quicksort_Reel_Entier is
            new Quicksort(Vector_Index => Vector_Entier,
                        Vector_Element => Vector_Reel);
        
        use Quicksort_Reel_Entier;

    begin

        initialize(pi, 1.0/T_Reel(N));
        put_line("initialized pi to 1/N");
        -- put(pi); new_line;

        identity(pi_index);
        put_line("initialized pi_index to identity");
        -- put(pi_index); new_line;

        if naif then
            declare
                -- on instancie le module générique Naif de Google
                package Google_Naif is
                    new Google.Naif(N => N,
                                    N_links => N_links,
                                    Vector_Element => Vector_Reel);
                use Google_Naif;

                -- définition de la matrice Google
                G: T_Google;
            begin

                initialize(G, 0.0);
                put_line("initialized G to zeros");
                -- put(G); new_line;
                
                new_line;
                create_Google(G, alpha, file);
                put_line("created G");

                close(file);
                put("closed "); put(To_String(filename & ".net")); new_line;
                new_line;

                -- on applique l'algorithme itératif
                for i in 1..ite_max loop
                    pi := pi * G;
                    String'Write(Ada.Text_IO.Text_Streams.Stream(stdout),
                                ASCII.CR & "ite:" & Integer'Image(i) & " /" & Integer'Image(ite_max));
                end loop;

                -- new_line;
                -- put_line("final pi:");
                -- put(pi); new_line;
            end;
        
        else -- not naif
            declare
                -- on instancie le module générique Creux de Google
                package Google_Creux is
                    new Google.Creux(N => N,
                                    N_links => N_links,
                                    Vector_Element => Vector_Reel);
                use Google_Creux;
                
                -- définition de la matrice Google
                G: T_Google;
            begin

                create_Google(G, file);
                put_line("created G");
                
                -- new_line; new_line;
                -- put(G);
                -- new_line; new_line;

                close(file);
                put("closed "); put(To_String(filename & ".net")); new_line;
                new_line;

                -- on applique l'algorithme itératif
                for i in 1..ite_max loop
                    pi := calcul(pi, G, alpha);
                    String'Write(Ada.Text_IO.Text_Streams.Stream(stdout),
                                ASCII.CR & "ite:" & Integer'Image(i) & " /" & Integer'Image(ite_max));
                end loop; new_line;

                -- new_line;
                -- put_line("final pi:");
                -- put(pi);
            end;
        end if;

        new_line;

        -- on trie pi avec ses indices
        sort(pi, pi_index);
        put_line("sorted pi and pi_index");
        flip(pi);
        put_line("reversed pi");
        flip(pi_index);
        put_line("reversed pi_index");

        new_line;

        -- on écrit les résultats dans les fichiers
        create(file, Out_File, To_String(filename & "_GH05.p"));
        put(file, N, 1); put(file, ' ');
        put(file, alpha, Fore=>1, Aft=>10); put(file, ' ');
        put(file, ite_max, 1); new_line(file);
        put(file, pi);
        close(file);
        put_line("wrote pi to " & To_String(filename & "_GH05.p"));
    
        create(file, Out_File, To_String(filename & "_GH05.ord"));
        put(file, pi_index);
        close(file);
        put_line("wrote pi_index to " & To_String(filename & "_GH05.ord"));
    end;

exception

    -- si il y a une erreur lors du parsing des arguments, on rappelle l'usage.
    when ERROR_args =>
        new_line;
        put_line("Erreur lors de la saisi de la commande.");
        put_line("Usage: pagerank [-n] [-i max_iterations] [-a alpha] [-h] fichier_reseau.net");

    when ERROR_alpha =>
        new_line;
        put_line("Erreur lors de la saisi de alpha.");
        put_line("alpha ∈ [0, 1]");

    when ERROR_iteration =>
        new_line;
        put_line("Erreur lors de la saisi de ite_max.");
        put_line("ite_max ∈ ⟦0, 150⟧");

    when ERROR_filename =>
        new_line;
        put_line("Erreur lors de la saisi du fichier réseau.");
        put_line("Veuillez rentrer un fichier existant.");

    when INFO_tips =>
        put_line("Essayez 'pagerank --help' pour plus d'informations.");

    when INFO_help =>
        new_line;
        put_line("Usage: pagerank [OPTIONS] network.net");
        put_line("Calcule le pagerank d'un réseau à partir de son réseau.net");
        new_line;
        put_line("Options:");
        put_line("  -n, --naif              specifies which type of matrix to use");
        put_line("  -a, --alpha             specifies the alpha constant (alpha ∈ [0, 1])");
        put_line("  -i, --ite-max           specifies the maximum number of iterations (ite_max ∈ ⟦0, 150⟧)");
        put_line("  -h, --help              display this help message and exit");

end pageRank;
