package body Google is

    package body Naif is

        function "*"(left : Vector_Element.T_Vecteur; right : T_Google) return Vector_Element.T_Vecteur is
            vec: Vector_Element.T_Vecteur;
            c: T_Reel;
        begin
            for i in 0..N-1 loop
                c := 0.0;
                for j in 0..N-1 loop
                    c := c + left(j)*right(j,i);
                end loop;
                vec(i) := c;
            end loop;
            return vec;
        end "*";

        procedure initialize(mat: in out T_Google; value: in T_Reel) is
        begin
            for i in 0..N-1 loop
                for j in 0..N-1 loop
                    mat(i,j) := value;
                end loop;
            end loop;
        end initialize;

        procedure create_H(mat: in out T_Google; file: in Ada.Text_IO.File_Type) is
            row, col: Natural;
            nc: Natural := 0;
            nl: Natural := 0;
        begin
            for i in 0..N_Links-1 loop
                get(file, row);
                get(file, col);
                mat(row, col) := 1.0;
            end loop;

            for i in 0..N-1 loop
                for j in 0..N-1 loop
                    exit when mat(i, j) /= 0.0;
                    if j = N-1 then
                        nl := nl + 1;
                    end if;
                end loop;
            end loop;
            put("lignes vides = "); put(nl, 1);
            new_line;

            for j in 0..N-1 loop
                for i in 0..N-1 loop
                    exit when mat(i, j) /= 0.0;
                    if i = N-1 then
                        nc := nc + 1;
                    end if;
                end loop;
            end loop;
            put("colonnes vides = "); put(nc, 1);
            new_line;

        end create_H;

        procedure create_S(mat: in out T_Google) is
            sum: T_Reel;
        begin
        for i in 0..N-1 loop
            sum := 0.0;
            for j in 0..N-1 loop
                sum := sum + mat(i,j);
            end loop;

            if sum /= 0.0 then
                for j in 0..N-1 loop
                    if mat(i,j) /= 0.0 then
                        mat(i,j) := 1.0/sum;
                    end if;
                end loop;
            else -- sum == 0 then
                for j in 0..N-1 loop
                    mat(i,j) := 1.0/T_Reel(N);
                end loop;
            end if;
        end loop;
        end create_S;

        procedure create_G(mat: in out T_Google; alpha: in T_Reel) is
        begin
            for i in 0..N-1 loop
                for j in 0..N-1 loop
                    mat(i,j) := alpha*mat(i,j) + (1.0-alpha)/T_Reel(N);
                end loop;
            end loop;
        end create_G;

        procedure create_Google(mat: in out T_Google; alpha: in T_Reel; file: in Ada.Text_IO.File_Type) is
        begin

            create_H(mat, file);
            create_S(mat);
            create_G(mat, alpha);

        end create_Google;

        procedure put(mat: in T_Google) is
        begin
            for i in 0..N-1 loop
                for j in 0..N-1 loop
                    put(mat(i,j));
                end loop;
                new_line;
            end loop;
        end put;

    end Naif;



    package body Creux is

        procedure create_Google(mat: in out T_Google; file: in out Ada.Text_IO.File_Type) is
            row, col: Natural;
        begin

            initialize(mat.Rows, 0);
            initialize(mat.Cols, N);

            for i in 0..N_Links-1 loop
                get(file, row);
                get(file, col);
                mat.rows(row+1) := mat.rows(row+1) + 1;
            end loop;

            repartition(mat.rows);

            reset(file, In_File);
            skip_line(file);

            for i in 0..N_Links-1 loop
                get(file, row);
                get(file, col);

                for j in mat.rows(row)..mat.rows(row+1)-1 loop

                    exit when mat.cols(j) = col; -- anti-doublon

                    if mat.cols(j) >= N then
                        mat.cols(j) := col;
                        exit;
                    end if;

                end loop;

            end loop;

        end create_Google;

        procedure put(mat: in T_Google) is
        begin
            put("rows:");
            for i in 0..N loop
                put(mat.rows(i));
            end loop;
            new_line; new_line;
            put("cols:");
            for i in 0..N_Links-1 loop
                put(mat.cols(i));
            end loop;
        end put;

        function calcul(vec: in Vector_Element.T_Vecteur; mat: in T_Google; alpha: in T_Reel) return Vector_Element.T_Vecteur is
            new_vec: Vector_Element.T_Vecteur;
            nb_col:  Natural;
            index_col: Integer;
        begin
            
            initialize(new_vec, sum(vec)*(1.0-alpha)/T_Reel(N));

            for i in 0..N-1 loop

                nb_col := mat.rows(i+1) - mat.rows(i);
                
                if nb_col = 0 then
                    
                    for j in 0..N-1 loop
                        new_vec(j) := new_vec(j) + alpha*vec(i)/T_Reel(N);
                    end loop;
                
                else
                
                    for j in mat.rows(i)..mat.rows(i+1)-1 loop
                        index_col := mat.cols(j);
                        if index_col < N then -- anti calcul d'un doublon
                            new_vec(index_col) := new_vec(index_col) + alpha*vec(i)/T_Reel(nb_col);
                        end if;
                    end loop;
                
                end if;
            
            end loop;
            
            return new_vec;
        end calcul;

    end Creux;

end Google;