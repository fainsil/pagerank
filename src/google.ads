with Vector;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

-- tester de mettre le generic ici ?

package Google is

    generic

        -- N peut être supprimé et recup direct grace vecteur_element
        N: Positive;
        N_Links: Positive;

        -- Vecteur contenant les élements du même type que la matrice
        with package Vector_Element is new Vector.Reel(<>);

    package Naif is

        use Vector_Element;
        use Vector_Element.Text_T_Reel;

        type T_Google is array (0..N-1, 0..N-1) of T_Reel;

        -- permet d'initialiser la matrice avec pour tout i,j, mat[i, j] = value
        procedure initialize(mat: in out T_Google; value: in T_Reel);

        -- permet la création de la matrice de Google, voir l'énoncé
        procedure create_Google(mat: in out T_Google; alpha: in T_Reel; file: in Ada.Text_IO.File_Type);

        -- permet le produit vecteur-matrice
        function "*"(left: Vector_Element.T_Vecteur; right: T_Google) return Vector_Element.T_Vecteur;

        -- permet l'affiche de la matrice
        procedure put(mat: in T_Google);

    end Naif;



    generic

        -- idem ici
        N: Positive;
        N_Links: Positive;

        -- Vecteur contenant les élements du même type que la matrice
        with package Vector_Element is new Vector.Reel(<>);

    package Creux is

        use Vector_Element;
        use Vector_Element.Text_T_Reel;

        package Vector_Rows is
            new Vector.Entier(T_Entier => Natural, Capacite => N+1);
        package Vector_Cols is
            new Vector.Entier(T_Entier => Natural, Capacite => N_Links);

        use Vector_Rows;
        use Vector_Cols;

        type T_Rows is array (0..N) of Natural;
        type T_Cols is array (0..N_Links-1) of Natural;

        -- on définit le type T_Google correspondant à une matrice CSC
        type T_Google is record
            rows: Vector_Rows.T_Vecteur;
            cols: Vector_Cols.T_Vecteur;
        end record;

        -- permet de créer la matrice à partir du réseau
        procedure create_Google(mat: in out T_Google; file: in out Ada.Text_IO.File_Type);  
        
        -- permet faire le ~produit vecteur-matrice
        function calcul(vec: in Vector_Element.T_Vecteur; mat: in T_Google; alpha: in T_Reel) return Vector_Element.T_Vecteur;
        
        -- permet l'affichage de la matrice, voir l'article wikiepdia pour comprendre format
        procedure put(mat: in T_Google);

    end Creux;

end Google;