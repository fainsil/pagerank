make:
	cd build/ && \
	gnat make -gnatwa -gnata -g ../src/*.adb

clean:
	cd build/ && \
	gnat clean ../src/*.adb && \
	rm ../fichiers_test/*/*_GH05*

test:
	bash test_pagerank.bash