#!/bin/bash

ulimit -s unlimited

echo "$(tput setaf 2)command: make clean$(tput setaf 7)"
make clean
echo "$(tput setaf 2)command: make$(tput setaf 7)"
make
echo

echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -h$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -h
echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net --help$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net --help

echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -a 0.1$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -a 0.1
echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net --alpha 0.1$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net --alpha 0.1

echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -i 100$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -i 100
echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net --ite-max 100$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net --ite-max 100

echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -i abcd$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -i abcd
echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -a abcd$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -a abcd
echo "$(tput setaf 2)command: build/pagerank$(tput setaf 7)"
build/pagerank
echo "$(tput setaf 2)command: build/pagerank -i 100$(tput setaf 7)"
build/pagerank -i 100



echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net --naif$(tput setaf 7)"
build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net --naif
echo
echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -n$(tput setaf 7)"
time build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net -n
echo "$(tput setaf 4)"
wdiff -s fichiers_test/Exemple_sujet/exemple_sujet_GH05.ord fichiers_test/Exemple_sujet/exemple_sujet.ord | tail -n 2
echo
wdiff -s fichiers_test/Exemple_sujet/exemple_sujet_GH05.p fichiers_test/Exemple_sujet/exemple_sujet_P_6.p | tail -n 2
echo "$(tput setaf 7)"



echo "$(tput setaf 2)command: build/pagerank fichiers_test/Worm/worm.net -n$(tput setaf 7)"
time build/pagerank fichiers_test/Worm/worm.net -n
echo "$(tput setaf 4)"
wdiff -s fichiers_test/Worm/worm_GH05.ord fichiers_test/Worm/worm.ord | tail -n 2
echo
wdiff -s fichiers_test/Worm/worm_GH05.p fichiers_test/Worm/worm_P_6.p | tail -n 2
echo "$(tput setaf 7)"



echo "$(tput setaf 2)command: build/pagerank fichiers_test/Brainlinks/brainlinks.net -n$(tput setaf 7)"
time build/pagerank fichiers_test/Brainlinks/brainlinks.net -n
echo "$(tput setaf 4)"
wdiff -s fichiers_test/Brainlinks/brainlinks_GH05.ord fichiers_test/Brainlinks/brainlinks.ord | tail -n 2
echo
wdiff -s fichiers_test/Brainlinks/brainlinks_GH05.p fichiers_test/Brainlinks/brainlinks_P_6.p | tail -n 2
echo "$(tput setaf 7)"



echo "$(tput setaf 2)command: build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net$(tput setaf 7)"
time build/pagerank fichiers_test/Exemple_sujet/exemple_sujet.net
echo "$(tput setaf 4)"
wdiff -s fichiers_test/Exemple_sujet/exemple_sujet_GH05.ord fichiers_test/Exemple_sujet/exemple_sujet.ord | tail -n 2
echo
wdiff -s fichiers_test/Exemple_sujet/exemple_sujet_GH05.p fichiers_test/Exemple_sujet/exemple_sujet_P_6.p | tail -n 2
echo "$(tput setaf 7)"



echo "$(tput setaf 2)command: build/pagerank fichiers_test/Worm/worm.net$(tput setaf 7)"
time build/pagerank fichiers_test/Worm/worm.net
echo "$(tput setaf 4)"
wdiff -s fichiers_test/Worm/worm_GH05.ord fichiers_test/Worm/worm.ord | tail -n 2
echo
wdiff -s fichiers_test/Worm/worm_GH05.p fichiers_test/Worm/worm_P_6.p | tail -n 2
echo "$(tput setaf 7)"



echo "$(tput setaf 2)command: build/pagerank fichiers_test/Brainlinks/brainlinks.net$(tput setaf 7)"
time build/pagerank fichiers_test/Brainlinks/brainlinks.net
echo "$(tput setaf 4)"
wdiff -s fichiers_test/Brainlinks/brainlinks_GH05.ord fichiers_test/Brainlinks/brainlinks.ord | tail -n 2
echo
wdiff -s fichiers_test/Brainlinks/brainlinks_GH05.p fichiers_test/Brainlinks/brainlinks_P_6.p | tail -n 2
echo "$(tput setaf 7)"

# echo "$(tput setaf 2)command: build/pagerank fichiers_test/Linux26/Linux26.net$(tput setaf 7)"
# time build/pagerank fichiers_test/Linux26/Linux26.net
# echo

echo "$(tput setaf 3)FIN DES TESTS$(tput setaf 7)"
